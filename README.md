# Projet PKI

Ce projet permet d'avoir une certificats "self-signed" afin d'avoir son propre PKI.

## Table des matières
- [Projet PKI](#projet-pki)
  - [Table des matières](#table-des-matières)
  - [Présentation](#présentation)
  - [Prérequis](#prérequis)
  - [Installation](#installation)
  - [Utilisation](#utilisation)
  - [License](#license)

## Présentation

[Lien de la vidéo](https://streamable.com/d1m6je) ou [Lien de la vidéo](https://www.youtube.com/watch?v=6adTbrp3kYM)

Ce projet fût réalisé avec docker et docker compose.
L'image utilisé est [alpine](https://hub.docker.com/_/alpine) avec comme dépendance nginx, nano, curl et openssl. [Dockerfile](Dockerfile)

Dans le [docker-compose.yml](docker-compose.yml) on retrouve uniquement le service pki.
Ce service ayant 3 volumes :
- [default.conf](default.conf) : configuration de nginx reporté dans `/etc/nginx/conf.d/default.conf`
- [ssl](ssl) : dossier contenant les certificats reporté dans `/etc/nginx/ssl`
- [index.html](index.html) : fichier du site web reporté dans `/var/www/monpki/http/index.html`

Dans le fichier [default.conf](default.conf) on retrouve la configuration de nginx avec le port 80 et 443.
Le premier server utilise le port 80 et vers le port 443.
Le second server utilise le port 443 est configuré avec le certificat [monpki.crt](ssl/monpki.crt).

Si on utilise la command `curl http://localhost` avec avec le serveur qui est commenté on obtient le résultat suivant :

```bash
$ curl http://localhost
<!DOCTYPE html>
...
```

Si on utilise la command `curl http://localhost` avec avec le premier serveur on obtient le résultat suivant :
```bash
$curl http://localhost
<html>
<head><title>301 Moved Permanently</title></head>
<body>
...
Le serveur nous indique que la page est redirigé vers le port 443.
```

mais lorsque nous utilisons la commande `curl https://localhost` alors que nous n'avons pas le certificat installé, nous obtenons le résultat suivant :
```bash
$ curl https://localhost
curl: (60) SSL certificate problem: self signed certificate
...
```

Il est possible d'y accéder en utilisant l'option `-k` ou `--insecure` mais cela n'est pas recommandé.

```bash
$ curl -k https://localhost
<!DOCTYPE html>
...
```

Il est donc nécessaire d'installer le certificat sur le client afin de pouvoir accéder au site web.
Pour chaque système d'exploitation il y a une procédure différente. Suive la procédure ci-dessous [Ajout du certificat](#ajout-du-certificat)

Une fois cette étape réalisé, nous debons ajouter le nom de domaine dans le fichier `/etc/hosts` afin de pouvoir accéder au site web.
```shell
$ sudo nano /etc/hosts
# Ajouter la ligne suivante
120.0.0.1 monpki.com
```

Pour être sur que la manipulation est bien réalisé, nous pouvons utiliser la commande `curl https://monpki.com` et nous obtenons le résultat suivant :
```bash
$ curl https://monpki.com
<!DOCTYPE html>
...
```

Nous avons donc accès au site web et nous pouvons voir le certificat utilisé en cliquant sur le cadenas à gauche de l'url.
Le site web est accessible via l'url [https://monpki.com](https://monpki.com)

Il est possible de modifier le certificat en modifiant le fichier [default.conf](default.conf) et en utilisant la commande suivante [Commande utilisé pour générer le certificat](#commande-utilisé-pour-générer-le-certificat-)

## Prérequis
- [Docker](https://docs.docker.com/engine/install/)

## Installation
Suivez les étapes ci-dessous pour installer et configurer le projet :
```shell
$ git clone https://gitlab.com/azdra/pki.git

$ cd pki
```

## Utilisation
#### Pour lancer l'application, exécutez la commande suivante :
```bash
$ docker compose build # build les dépendance
$ docker compose up -d # start le projet en mode détaché
$ docker compose exec pki sh # se connecter au container
```


#### Commande utilisé pour générer le certificat :
```bash
openssl req -x509 -nodes -days 365 -subj "/C=CA/ST=QC/O=Company, Inc./CN=monpki.com" -addext "subjectAltName=DNS:monpki.com" -newkey rsa:2048 -keyout /etc/nginx/ssl/monpki.key -out /etc/nginx/ssl/monpki.crt;
```
Pour générer le certificat, nous utilisons la commande `openssl req` avec les options suivantes :
- `-x509` : Spécifie que la sortie doit être un certificat auto-signé au format X.509, plutôt qu'une demande de certificat.
- `-nodes` : Indique que la clé privée ne doit pas être cryptée par un mot de passe. Cela signifie qu'aucun mot de passe ne sera demandé lors de l'utilisation du certificat.
- `-days` **365** : Définit la durée de validité du certificat en jours. Dans cet exemple, le certificat serait valable pendant un an.
- `-subj` **/C=CA/ST=QC/O=Company, Inc./CN=monpki.com** : Fournit les informations du sujet pour le certificat :
  - **/C=CA** : Pays (Canada)
  - **/ST=QC** : Province (Québec)
  - **/O=Company, Inc.** : Organisation (Company, Inc.)
  - **/CN=monpki.com** : Nom de domaine pour lequel le certificat est émis (dans ce cas, monpki.com).
- `-addext` **subjectAltName=DNS:monpki.com** :  Ajoute une extension au certificat pour spécifier les noms alternatifs du sujet. Cela inclut le nom de domaine monpki.com comme alternative au nom commun spécifié précédemment.
- `-newkey` **rsa:2048** : Génère une nouvelle clé privée RSA de 2048 bits.
- `-keyout` : Indique le chemin où la clé privée générée sera enregistrée.
- `-out` : Indique le chemin où le certificat auto-signé généré sera enregistré.

## Ajout du certificat
Pour ajouter le certificat sur un client uniquement pour Linux. La windoberie et macconnerie se débrouillent.
```bash
$ sudo mkdir -p /usr/local/share/ca-certificates/monpki
sudo cp ssl/monpki.crt /usr/local/share/ca-certificates/monpki/
sudo update-ca-certificates
certutil -A -n "esx1.monpki.lan" -t "C," -i /usr/local/share/ca-certificates/monpki/monpki.crt -d sql:$HOME/.pki/nssdb/
```
<ul>
<li>Pour la première command nous allons tout simplement créer un nouveau dossier dans `/usr/local/share/ca-certificates/` avec le nom de notre PKI pour le ranger et le retrouver plus facilement.</li>
<li>La second pour copier notre certificat dans le dossier que nous venons de créer.</li>
<li>La troisième pour mettre à jour la liste des certificats.</li>
<li>La quatrième pour ajouter le certificat dans la base de donnée de certificat des navigateurs web</li>
</ul>


## Suppression du certificat

Pour supprimer le certficat : 

```bash
$ sudo rm -rf /usr/local/share/ca-certificates/monpki/monpki.crt
sudo update-ca-certificates -f
certutil -d sql:$HOME/.pki/nssdb -D -n esx1.monpki.lan
```
<ul>
<li>La première command permet de supprimer le certificat du dossier `/usr/local/share/ca-certificates/monpki/monpki.crt`.</li>
<li>La seconde permet de mettre à jour la liste des certificats.</li>
<li>La troisième permet de supprimer le certificat de la base de donnée de certificat des navigateurs web.</li>
</ul>

## Auteurs

Ce projet a été développé par [Baptiste](https://gitlab.com/azdra).

## Licence

Ce projet est sous licence [MIT](LICENSE). Vous pouvez consulter le fichier [LICENSE](LICENSE) pour plus de détails.

