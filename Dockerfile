# Utilisez l'image Alpine comme image de base
FROM alpine:latest

# Mettez à jour les paquets et installez Nginx, nano curl et openssl
RUN apk update && apk upgrade && apk add nginx nano curl openssl

# Expose ports 80 and 443 for HTTP and HTTPS
EXPOSE 80
EXPOSE 443

# Commande pour démarrer alipin lorsque le conteneur démarre
CMD ["tail", "-f", "/dev/null"]
